def fn_failure_response(data):
    response = dict()
    response["api_status"] = {
        "status": "Failure",
        "message": data.get('manual_error_messsage')
    }
    response["api_error"] = {
        "message": data.get('manual_error_messsage')
    }
    response["sproc_oparams"] = {
        "oparam_err_flag": 1,
        "oparam_err_step": None,
        "oparam_err_msg": data.get('manual_error_messsage')
    }
    return response
