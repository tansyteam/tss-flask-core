from tss_flask_core.db.executeSProc import fn_call_stored_procedure, fn_return_sproc_multiple_result_sets

def fn_get_micro_service_url(kwargs):
    try:
        input_params = []
        output_params = [
            'oparam_err_flag',
            'oparam_err_step',
            'oparam_err_msg'
        ]
        sproc_result_args, arg_names, cursor = fn_call_stored_procedure(kwargs["conn"],
                                                                        kwargs["client_name"],
                                                                        'sproc_csm_com_lkp_microservice_url',
                                                                        *input_params,
                                                                        *output_params,
                                                                        return_arg_names=True)

        sproc_data = fn_return_sproc_multiple_result_sets(sproc_result_args=sproc_result_args,
                                                          arg_names=arg_names,
                                                          cursor=cursor,
                                                          functionality="micro service url fetched successfully")

        microservice_url = sproc_data[0]["data0"][0]
        return microservice_url
    except Exception as e:
        return str(e)