import requests
from datetime import datetime
import json

from tss_flask_core.microServices.microServiceUrl import fn_get_micro_service_url
from tss_flask_core.dbLog.phoneSubscriptionArn import fn_com_aws_phone_subscription_arn


# Create a topic
def fn_aws_sns_create_topic(kwargs, name, display_name):
    try:
        micro_service_details = fn_get_micro_service_url(kwargs)
        micro_service_url = micro_service_details.get('microservice_url', None)
        micro_api_key = micro_service_details.get('api_key', None)

        url = micro_service_url + "/sms/topic"
        headers = {
            'Content-Type': 'application/json',
            "x-api-key": micro_api_key
        }
        payload = {
            "name": name.replace(" ", ""),  # All Students => AllStudents
            "display_name": display_name.replace(" ", "")
        }
        res = requests.post(url, json=payload, headers=headers)
        return res.json()
    except Exception as e:
        return str(e)

# fn_aws_sns_create_topic("tss-client-topic-01", "topic-01")

# Delete a topic
def fn_aws_sns_delete_topic(kwargs, topic_arn):
    try:
        micro_service_details = fn_get_micro_service_url(kwargs)
        micro_service_url = micro_service_details.get('microservice_url', None)
        micro_api_key = micro_service_details.get('api_key', None)
        url = micro_service_url + "/sms/topic"
        headers = {
            'Content-Type': 'application/json',
            "x-api-key": micro_api_key
        }
        payload = {
            "topic_arn": topic_arn
        }
        res = requests.delete(url, json=payload, headers=headers)
        return res.json()
    except Exception as e:
        return str(e)

# fn_aws_sns_delete_topic("arn:aws:sns:ap-south-1:172496491963:tss-client-topic-01")

# suscribe phone number to topic
def fn_aws_sns_topic_subscribe(micro_service_url, topic_arn, phone_no):
    try:
        url = micro_service_url + "/sms/topic/subscribe"
        headers = {'Content-Type': 'application/json'}
        payload = {
            "topic_arn": topic_arn,
            "phone_no": phone_no
        }
        res = requests.post(url, json=payload, headers=headers)
        return res.json()
    except Exception as e:
        return str(e)

# fn_aws_sns_topic_subscribe("arn:aws:sns:ap-south-1:172496491963:tss-client-topic-01", "+918801933344")

# suscribe phone number to topic
def fn_aws_sns_topic_subscribe_multiple(kwargs, subscribe_data):
    try:
        micro_service_details = fn_get_micro_service_url(kwargs)
        micro_service_url = micro_service_details.get('microservice_url', None)
        micro_api_key = micro_service_details.get('api_key', None)

        url = micro_service_url + "/sms/topic/subscribe-multiple"
        headers = {
            'Content-Type': 'application/json',
            "x-api-key": micro_api_key
        }
        payload = {
            "subscribe_data": subscribe_data
        }
        res = requests.post(url, json=payload, headers=headers)
        topic_subscribe_response = res.json()

        if isinstance(topic_subscribe_response, dict):
            return topic_subscribe_response.get("error"), 400
        else:
            kwargs["topic_subscribe_response"] = topic_subscribe_response
            topic_subscribe_err_flag, topic_subscribe_status_code = fn_com_aws_phone_subscription_arn(kwargs)

            return topic_subscribe_err_flag, topic_subscribe_status_code
    except Exception as e:
        return str(e), 400

# fn_aws_sns_topic_subscribe_multiple("arn:aws:sns:ap-south-1:172496491963:tss-client-topic-01", ["+918801933344", "+918686381938"])

# Unsuscribe phone number to topic
def fn_aws_sns_topic_unsubscribe(micro_service_url, subscription_arn):
    try:
        url = micro_service_url + "/sms/topic/unsubscribe"
        headers = {'Content-Type': 'application/json'}
        payload = {
            "subscription_arn": subscription_arn
        }
        res = requests.delete(url, json=payload, headers=headers)
        return res.json()
    except Exception as e:
        return str(e)

# fn_aws_sns_topic_unsubscribe("arn:aws:sns:ap-south-1:172496491963:tss-client-topic-01")


# Unsuscribe multiple phone number to topic
def fn_aws_sns_topic_unsubscribe_multiple(kwargs, subscription_arns):
    try:
        micro_service_details = fn_get_micro_service_url(kwargs)
        micro_service_url = micro_service_details.get('microservice_url', None)
        micro_api_key = micro_service_details.get('api_key', None)

        url = micro_service_url + "/sms/topic/unsubscribe-multiple"
        headers = {
            'Content-Type': 'application/json',
            "x-api-key": micro_api_key
        }
        payload = {
            "subscription_arns": subscription_arns
        }
        res = requests.delete(url, json=payload, headers=headers)
        topic_unsubscribe_response = res.json()
        if isinstance(topic_unsubscribe_response, dict):
            return topic_unsubscribe_response.get("error"), 400
        else:
            return topic_unsubscribe_response, 200
    except Exception as e:
        return str(e), 400

# fn_aws_sns_topic_unsubscribe_multiple("arn:aws:sns:ap-south-1:172496491963:tss-client-topic-01")


# send SMS without topic (with phone)
def fn_aws_sns_send_sms_phone(kwargs, message, subject, phone_number):
    try:
        micro_service_details = fn_get_micro_service_url(kwargs)
        micro_service_url = micro_service_details.get('microservice_url', None)
        micro_api_key = micro_service_details.get('api_key', None)

        url = micro_service_url + "/sms/publish"
        headers = {
            'Content-Type': 'application/json',
            "x-api-key": micro_api_key
        }
        payload = {
            "message": message,
            "subject": subject,
            "phone_number": phone_number
        }
        res = requests.post(url, json=payload, headers=headers)
        sms_sent_response = res.json()
        sms_status_code = sms_sent_response['ResponseMetadata'].get('HTTPStatusCode', None)

        sns_date = datetime.strptime(sms_sent_response['ResponseMetadata']['HTTPHeaders']['date'],
                                                                    '%a, %d %b %Y  %H:%M:%S GMT').strftime(
                '%Y-%m-%d %H:%M:%S')

        sms_response_json = {
            "status_code": sms_status_code,
            "sns_date": sns_date,
            "request_id": sms_sent_response['ResponseMetadata'].get('RequestId', None),
            "message_id": sms_sent_response.get('MessageId', None)
        }

        sms_response = dict()
        if sms_sent_response['ResponseMetadata'].get('HTTPStatusCode', None) == 200:
            sms_response["sms_response_json"] = sms_response_json
            sms_response["api_oparams"] = {}
            sms_response["api_status"] = {
                "status": "Success",
                "message": "SMS sent successfully"
            }
        else:
            sms_response["sms_sent_response"] = sms_sent_response
            sms_response["api_oparams"] = {}
            sms_response["api_status"] = {
                "status": "Failure",
                "message": "SMS not sent"
            }
        kwargs['provider_request_id'] = sms_sent_response['ResponseMetadata'].get('RequestId', None)
        kwargs['provider_message_id'] = sms_sent_response.get('MessageId', None)

        kwargs['parent_entity_id'] = None

        kwargs['mobile'] = phone_number
        kwargs['message'] = message
        kwargs['sms_timestamp'] = sns_date
        kwargs['send_status'] = "OK"

        sms_log_err_flag, sms_log_status_code = fn_com_sms_response_log(kwargs)
        return sms_response, sms_status_code
    except Exception as e:
        return str(e), 400

# fn_aws_sns_send_sms_without_topic("hello world", "testing", "+918686381938")        

# send SMS with topic
def fn_aws_sns_send_sms_topic(kwargs, message, subject, topic_arn):
    try:
        micro_service_details = fn_get_micro_service_url(kwargs)
        micro_service_url = micro_service_details.get('microservice_url', None)
        micro_api_key = micro_service_details.get('api_key', None)

        url = micro_service_url + "/sms/topic/publish"
        headers = {
            'Content-Type': 'application/json',
            "x-api-key": micro_api_key
        }
        payload = {
            "message": message,
            "subject": subject,
            "topic_arn": topic_arn
        }
        res = requests.post(url, json=payload, headers=headers)
        sms_sent_response = res.json()
        sms_status_code = sms_sent_response['ResponseMetadata'].get('HTTPStatusCode', None)

        sns_date = datetime.strptime(sms_sent_response['ResponseMetadata']['HTTPHeaders']['date'],
                                     '%a, %d %b %Y  %H:%M:%S GMT').strftime('%Y-%m-%d %H:%M:%S')

        sms_response_json = {
            "status_code": sms_status_code,
            "sns_date": sns_date,
            "request_id": sms_sent_response['ResponseMetadata'].get('RequestId', None),
            "message_id": sms_sent_response.get('MessageId', None)
        }

        sms_response = dict()
        if sms_sent_response['ResponseMetadata'].get('HTTPStatusCode', None) == 200:
            sms_response["sms_response_json"] = sms_response_json
            sms_response["api_oparams"] = {}
            sms_response["api_status"] = {
                "status": "Success",
                "message": "Batch SMS sent successfully"
            }
        else:
            sms_response["sms_sent_response"] = sms_sent_response
            sms_response["api_oparams"] = {}
            sms_response["api_status"] = {
                "status": "Failure",
                "message": "Batch SMS not sent"
            }

        log_json_sms_sent = [{"entity_id": None,
                              "mobile": None,
                              "message": message,
                              "sms_timestamp": sns_date,
                              "send_status": "OK"
                              }]
        # kwargs['entity_id'] = None
        kwargs['provider'] = "AWS"

        kwargs['total_sms_in_batch'] = None
        kwargs['success_count'] = None
        kwargs['failure_count'] = None

        kwargs['provider_batch_credits'] = 0
        kwargs['sms_batch_flag'] = 1

        kwargs['provider_request_id'] = sms_sent_response['ResponseMetadata'].get('RequestId', None)
        kwargs['provider_message_id'] = sms_sent_response.get('MessageId', None)

        kwargs['log_json_sms_sent'] = json.dumps(log_json_sms_sent)

        kwargs['log_json_sms_received'] = None
        kwargs['common_message'] = message

        kwargs['product_entity_id'] = None

        # kwargs['mobile'] = None
        # kwargs['message'] = message
        kwargs['sms_timestamp'] = sns_date
        kwargs['send_status'] = "OK"

        sms_log_response, sms_log_status_code = fn_com_sms_response_log(kwargs)
        if sms_log_status_code == 400:
            return sms_log_response, sms_log_status_code
        else:
            return sms_response, sms_status_code
    except Exception as e:
        return str(e), 400

# fn_aws_sns_send_sms_with_topic("hello world", "testing", "arn:aws:sns:ap-south-1:172496491963:tss-client-topic-01") 


# send SMS with single/multiple phone numbers
def fn_aws_sns_send_sms_multiple_phones(kwargs, sms_send_data):
    try:
        micro_service_details = fn_get_micro_service_url(kwargs)
        micro_service_url = micro_service_details.get('microservice_url', None)
        micro_api_key = micro_service_details.get('api_key', None)

        url = micro_service_url + "/sms/publish-multiple"
        headers = {
            'Content-Type': 'application/json',
            "x-api-key": micro_api_key
        }
        payload = {
            "sms_send_data": sms_send_data
        }
        res = requests.post(url, json=payload, headers=headers)
        sms_sent_response = res.json()

        kwargs['provider_request_id'] = None
        kwargs['provider_message_id'] = None

        kwargs['log_json_sms_sent'] = json.dumps(sms_sent_response)

        kwargs['log_json_sms_received'] = None
        kwargs['common_message'] = None

        # Check product entity id in all response logs
        kwargs['product_entity_id'] = kwargs['product_entity_ids'] if 'product_entity_ids' in kwargs else None

        kwargs['mobile'] = None
        kwargs['message'] = None
        kwargs['sms_timestamp'] = None
        kwargs['send_status'] = "OK"

        sms_log_response, sms_log_status_code = fn_com_sms_response_log(kwargs)

        if sms_log_status_code == 400:
            return sms_log_response, sms_log_status_code
        else:
            return sms_log_response, 200
    except Exception as e:
        return str(e), 400

