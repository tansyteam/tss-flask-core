import mysql.connector
import json
import os
import datetime
from tss_flask_core.serialize import clsCustomJSONEncoder
from flask import request

def fn_get_sproc_argnames(cursor, sproc_name, client_db_name):
    query = "SELECT parameter_name  FROM information_schema.parameters  " \
                   "WHERE SPECIFIC_SCHEMA = '{}' " \
                   "and SPECIFIC_NAME = '{}' " \
                   "ORDER BY ordinal_position".format(client_db_name[0],sproc_name)

    cursor.execute(query)
    return [x for x, in cursor.fetchall()]

def fn_get_sproc_output_params(cursor, sproc_name, client_db_name):
    query = "SELECT PARAMETER_NAME FROM information_schema.parameters " \
            "WHERE SPECIFIC_SCHEMA = '{}' " \
            "and PARAMETER_NAME like '%oparam%' " \
            "and SPECIFIC_NAME = '{}' ORDER BY ordinal_position".format(client_db_name,sproc_name)

    cursor.execute(query)
    return [x for x, in cursor.fetchall()]


# execute stored procedure
def fn_call_stored_procedure(connection, client_db_name, sproc_name, *args, return_arg_names=False):
    """
        return_column_names - append arg_names to result
    """
    SPROC_SAMPLES = False
    try:
        cursor = connection.cursor()
        columns = None

        sproc_result_args = cursor.callproc(sproc_name, args)

        if return_arg_names:
            columns = fn_get_sproc_output_params(cursor, sproc_name, client_db_name)

            if SPROC_SAMPLES == True:
                sproc_params = fn_get_sproc_argnames(cursor, sproc_name, client_db_name)
                fn_create_sproc_samples(cursor, sproc_name, sproc_result_args, sproc_params, client_db_name)

            length_of_input_params = len(sproc_result_args) - len(columns)
            sproc_output_values = sproc_result_args[length_of_input_params:]
            return sproc_output_values, columns, cursor

        return sproc_result_args, cursor
    except mysql.connector.Error as error:
        if return_arg_names:
            print('Failed to execute stored procedure: {0}'.format(error))
            return str(error), columns, 400
        else:
            print('Failed to execute stored procedure: {0}'.format(error))
            return str(error), 400

def fn_create_sproc_samples(cursor, sproc_name, sproc_result_args, columns, client_db_name):
    oparam_names = fn_get_sproc_output_params(cursor, sproc_name, client_db_name)
    length_output_params = len(oparam_names)
    input_param_values = list(sproc_result_args)[:-length_output_params]
    input_param_names = columns[:-length_output_params]
    sproc_params = input_param_names + oparam_names

    list_of_input_params = ''.join(['SET @' + str(name) + ' = ' + str(input_param_values[index]) + '; \n'
                                    for index, name in enumerate(input_param_names)])

    cal_sproc = '\n,'.join(['@' + str(name) for name in sproc_params])
    cal_mysql_sproc = '\n' + 'CALL ' + sproc_name + '(' + cal_sproc + ')' + '; \n\n'

    select_output_params = '\n,'.join(['@' + str(name) for name in oparam_names]) + '; \n'
    select_mysql_sproc = 'SELECT ' + select_output_params

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    LOGS_DB_PATH = os.path.join(BASE_DIR, 'logging')

    if (not os.path.exists(LOGS_DB_PATH)):
        os.makedirs(LOGS_DB_PATH)

    now = datetime.datetime.now()
    fname = format(now, '%H:%M') + '_log.txt'
    fpath = os.path.join(LOGS_DB_PATH, fname)
    file = open(fpath, 'a+')
    try:
        os.chmod(fpath, 0o777)
        file.write(list_of_input_params)
        file.write(cal_mysql_sproc)
        file.write(select_mysql_sproc)
        file.write("---------------------------------------------------------------------- \n")
    except:
        pass

    file.close()




# capture single result set from the stored procedure execution
def fn_sproc_response(cursor):
    for result in cursor.stored_results():
        return result.fetchall()

# capture multiple single result set from the stored procedure execution
def fn_sproc_multiple_result_sets_response(cursor):
    multiple_result = []
    for result in cursor.stored_results():
        res = result.fetchall()
        rowncols = [dict(zip(result.column_names, x)) for x in res]
        multiple_result.append(rowncols)
    return multiple_result

def fn_get_sproc_errors(**kwargs):
    sproc_result_args_type = isinstance(kwargs['sproc_result_args'], str)
    if sproc_result_args_type == True and kwargs['cursor'] == 400:
        return 'Failure', kwargs['sproc_result_sets'], 400
    elif kwargs["argdict"].get('oparam_err_flag') == 1 or kwargs["argdict"].get('oparam_err_flag') == None:
        return 'Failure', kwargs["argdict"], 400
    else:
        return 'Success', kwargs['cursor'], 200

def fn_sprocresult2datas(sproc_result_sets):
    ret = {}
    for n, set_ in enumerate(sproc_result_sets):
        ret["data%s"%n] = set_

    serialized = json.dumps(ret, cls=clsCustomJSONEncoder)
    deserialized = json.loads(serialized)

    return deserialized


def fn_response_data(**kwargs):
    if kwargs['method'] == "GET":
        response = dict()
        response["status"] = 'Success'
        response["message"] = kwargs['functionality']
        response["api_status"] = {
            'status': 'Success',
            'message': kwargs['functionality']
        }
        if kwargs["args"]:
            # response["api_status"] = kwargs["args"]
            response["Sproc_OParams"] = kwargs["args"]
        response.update(fn_sprocresult2datas(kwargs["sproc_result_sets"]))
        return response, kwargs['status_code']
    response = dict()
    response["api_status"] = {
            'status': 'Success',
            'message': kwargs['functionality']
    }
    return response, kwargs['status_code']

def fn_return_sproc_multiple_result_sets(**kwargs):
    if (kwargs['cursor'] == 400):
        if isinstance(kwargs.get('sproc_result_args'), str):
            response = dict()
            response["status"] = 'Failure'
            response["message"] = kwargs.get('sproc_result_args', None)
            response["api_status"] = {
                'status': 'Failure',
                'message': kwargs.get('sproc_result_args', None)
            }
            response["Sproc_OParams"] = { 
                "oparam_err_msg": kwargs.get('sproc_result_args'),
                "oparam_err_flag": 1
            }
            response["args"] = { 
                "oparam_err_msg": kwargs.get('sproc_result_args'),
                "oparam_err_flag": 1
            }
            
            return response, 404
        else:
            response = dict()
            response["status"] = 'Failure'
            response["message"] = 'Something went wrong'
            response["args"] = kwargs
            response["api_status"] = {
                'status': 'Failure',
                'message': "Something went wrong"
            }
            response["Sproc_OParams"] = { 
                "oparam_err_msg": kwargs.get('sproc_result_args'),
                "oparam_err_flag": 1
            }
            return response, 404
    else:
        if kwargs["arg_names"]:
            kwargs["argdict"] = dict(zip(kwargs["arg_names"], kwargs["sproc_result_args"]))

        status, cursor_object, status_code = fn_get_sproc_errors(**kwargs)
        if status == "Failure" and status_code == 400:
            response = dict()
            response["api_status"] = {
                'status': 'Failure',
                'message': cursor_object.get("oparam_err_msg", None)
            }
            response["Sproc_OParams"] = cursor_object
            response["args"] = cursor_object
            response["status"] = status
            response["message"] = cursor_object.get("oparam_err_msg", None)
            
            return response, status_code
        else:
            sproc_multiple_result_sets = fn_sproc_multiple_result_sets_response(cursor_object)

            if kwargs["argdict"].get('oparam_err_flag') == 1:
                response = dict()
                response["status"] = status
                response["message"] = kwargs["argdict"].get('oparam_err_msg')
                response["api_status"] = {
                    'status': status,
                    'message': kwargs["argdict"].get('oparam_err_msg')
                }
                response["Sproc_OParams"] = { 
                    'err_flag': kwargs["argdict"].get('oparam_err_flag'),
                    'err_step': kwargs["argdict"].get('oparam_err_step'),
                    'err_msg': kwargs["argdict"].get('oparam_err_msg')
                }
                response["args"] = {
                    'err_flag': kwargs["argdict"].get('oparam_err_flag'),
                    'err_step': kwargs["argdict"].get('oparam_err_step'),
                    'err_msg': kwargs["argdict"].get('oparam_err_msg')
                }
                return response, status_code
            else:
                return fn_response_data(sproc_result_sets=sproc_multiple_result_sets,
                                        functionality=kwargs['functionality'],
                                        args=kwargs["argdict"],
                                        method="GET",
                                        status_code=status_code)

def fn_get_sproc_lookup_errors(**kwargs):
    sproc_result_args_type = isinstance(kwargs['sproc_result_args'], str)
    if sproc_result_args_type == True and kwargs['cursor'] == 400:
        return 'Failure', kwargs['sproc_result_sets'], 400
    else:
        return 'Success', kwargs['cursor'], 200

def fn_return_sproc_lookup_result_sets(**kwargs):
    if (kwargs['cursor'] == 400):
        response = dict()
        response["status"] = "Failure"
        response["message"] =  kwargs['sproc_result_args']
        response["api_status"] = {
            'status': "Failure",
            'message':  kwargs['sproc_result_args']
        }
        return response, 404
    else:
        if kwargs["arg_names"]:
            kwargs["argdict"] = dict(zip(kwargs["arg_names"], kwargs["sproc_result_args"]))

        status, cursor_object, status_code = fn_get_sproc_lookup_errors(**kwargs)
        if status == "Failure" and status_code == 400:
            response = dict()
            response["status"] = status
            response["message"] =  cursor_object
            response["api_status"] = {
                'status': status,
                'message':  cursor_object
            }
            return response, status_code
        else:
            sproc_multiple_result_sets = fn_sproc_multiple_result_sets_response(cursor_object)

            return fn_response_data(sproc_result_sets=sproc_multiple_result_sets,
                                    functionality=kwargs['functionality'],
                                    args="",
                                    method="GET",
                                    status_code=status_code)

def fn_s3_response(sproc_data, result, error_code, message):
    response = dict()
    response["Sproc_OParams"] = {
        "oparam_err_flag": sproc_data[0]["Sproc_OParams"].get("oparam_err_flag"),
        "oparam_err_step": sproc_data[0]["Sproc_OParams"].get("oparam_err_step"),
        "oparam_err_msg": sproc_data[0]["Sproc_OParams"].get("oparam_err_msg")
    }
    if sproc_data[0]["Sproc_OParams"].get("oparam_err_flag") == 0:
        if error_code == 404:
            response["api_status"] = {
                "message": result,
                "status": "Failure"
                }
            response["api_error"] = {
                "error_message": result
                }
            return response, 404
        
        response["api_status"] = {
            "message": message,
            "status": "Success" }

        response["api_oparams"] = {
            "base64_file": result.get('image_url'),
            # "data": sproc_data[0].get('data0')[0]
        }
    else:
        response["Sproc_OParams"] = {
        "oparam_err_flag": sproc_data[0]["Sproc_OParams"].get("oparam_err_flag"),
        "oparam_err_step": sproc_data[0]["Sproc_OParams"].get("oparam_err_step"),
        "oparam_err_msg": sproc_data[0]["Sproc_OParams"].get("oparam_err_msg")
        }
        response["api_error"] = {
            "error_message": sproc_data[0]["Sproc_OParams"].get("oparam_err_msg")
        }

        response["api_status"] = {
            "message": "File uploaded failed",
            "status": "Failure"
            }
        

    return response

def fn_exception_error_handling(error):
    response = dict()
    response["Sproc_OParams"] = {
        "oparam_err_flag": 1,
        "oparam_err_msg": error
        }
    response["api_error"] = {
        "error_message": error
    }

    response["api_status"] = {
        "message": "Some thing went wrong",
        "status": "Failure"
    }
        

    return response