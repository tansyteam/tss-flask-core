import json

from tss_flask_core.db.executeSProc import fn_call_stored_procedure, fn_return_sproc_multiple_result_sets


def fn_com_aws_phone_subscription_arn(kwargs):
    try:
        input_params = [
            json.dumps(kwargs['topic_subscribe_response'])
        ]

        output_params = [
            'oparam_err_flag',
            'oparam_err_step',
            'oparam_err_msg'
        ]

        sproc_result_args, arg_names, cursor = fn_call_stored_procedure(kwargs["conn"],
                                                                        kwargs["client_name"],
                                                                        'sproc_csm_com_aws_phone_subscription_arn_dml_upd',
                                                                        *input_params,
                                                                        *output_params,
                                                                        return_arg_names=True)

        sproc_data = fn_return_sproc_multiple_result_sets(sproc_result_args=sproc_result_args,
                                                          arg_names=arg_names,
                                                          cursor=cursor,
                                                          functionality="AWS subscription log saved successfully")

        if sproc_data[0]["Sproc_OParams"].get("oparam_err_flag", None) == 0:
            oparam_err_flag = sproc_data[0]["Sproc_OParams"].get("oparam_err_flag", None)

            return oparam_err_flag, 200
        else:
            oparam_err_flag = sproc_data[0]["Sproc_OParams"].get("oparam_err_flag", None)

            return oparam_err_flag, 400


    except Exception as e:
        oparam_err_flag = 1

        return oparam_err_flag, 400
