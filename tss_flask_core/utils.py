import functools

from datetime import datetime, timedelta
import mysql.connector
from flask import request, abort
from tss_flask_core.aws.aws_ssm import fetch_ssm_config
from tss_flask_core.failure_response import fn_failure_response


def generate_timestamp():
    return datetime.utcnow().isoformat()


def generate_local_timestamp():
    return datetime.utcfromtimestamp(datetime.now().timestamp() + 7200). \
                strftime('%Y-%m-%d %H:%M:%S')


def generate_expiry_date(year: int):
    return datetime.now() + timedelta(days=year * 365)


def generate_unique_key(key: str):
    t = str(datetime.now().timestamp()).split(".")
    ts = t[0] + t[1]
    return key[:24] + ts


def execute(fn):
    def wrapper(query_obj):
        host = fetch_ssm_config(f'/tss/prod/connection/host')
        origin = request.headers.get('Origin')
        if not origin:
            abort(400, "Missing origin header")
        if 'localhost' in request.headers.get('Origin'):
            client_name = request.headers.get('x-client_name')
            if not client_name:
                abort(400, "Client not found")
        else:
            try:
                client_name = origin.split('//')[1].split('.')[0]
            except IndexError:
                client_name = origin.split('.')[0]
            if 'staging' in origin:
                client_name = f"{client_name}_dev"
                host = fetch_ssm_config(f'/tss/dev/connection/host')
        username = fetch_ssm_config(f'/tss/{client_name}/connection/username')
        password = fetch_ssm_config(f'/tss/{client_name}/connection/password')
        db_name = fetch_ssm_config(f'/tss/{client_name}/connection/dbname')

        if not username or not password or not db_name:
            abort(400, "Client not found")
        conn = mysql.connector.connect(host=host,
                                       database=db_name,
                                       user=username,
                                       password=password)
        try:
            cursor = conn.cursor()
            query = fn(query_obj)
            cursor.execute(query, query_obj)
            result = [dict(zip([x[0] for x in cursor.description], row)) for row in cursor.fetchall()]
            cursor.close()
            conn.close()
            return result
        except:
            return [{"msg": "Database error"}]
    return wrapper


def fn_make_client_db_connection(function):
    """
    check db connection
    """
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        origin = request.headers.get('Origin')
        if not origin:
            data = {
                "manual_error_messsage": "Missing origin header"
            }
            response = fn_failure_response(data)
            return response, 400
        if 'localhost' in request.headers.get('Origin'):
            client_name = request.headers.get('x-client_name')
            if not client_name:
                data = {
                    "manual_error_messsage": "Client not found"
                }
                response = fn_failure_response(data)
                return response, 400
        else:
            try:
                client_name = origin.split('//')[1].split('.')[0]
            except IndexError:
                client_name = origin.split('.')[0]
            if 'staging' in origin:
                client_name = f"{client_name}_dev"
        
        host = fetch_ssm_config(f'/tss/dev/connection/host')
        username = fetch_ssm_config(f'/tss/{client_name}/connection/username')
        password = fetch_ssm_config(f'/tss/{client_name}/connection/password')
        db_name = fetch_ssm_config(f'/tss/{client_name}/connection/dbname')

        if not username or not password or not db_name:
            data = {
                "manual_error_messsage": "Client not found"
            }
            response = fn_failure_response(data)
            return response, 400
            
        try:
            conn = mysql.connector.connect(host=host,
                                    database=db_name,
                                    user=username,
                                    password=password)
            kwargs["conn"] = conn
            kwargs["client_name"] = db_name
            return function(request, *args, **kwargs)
        except Exception as e:
            data = {
                "manual_error_messsage": "Database error" + " - " + str(e)
            }
            response = fn_failure_response(data)
            return response, 400

    return wrapper