from tss_flask_core.microServices.microServiceCalls import fn_aws_sns_topic_subscribe_multiple

def fn_aws_sns_phone_topic_subscribe(kwargs, sproc_data, method):
    if method == "POST":
        subscribe_data = sproc_data[0]["data0"]
    else:
        subscribe_data = sproc_data[0]["data1"]
    if subscribe_data:
        subscribe_data_json = [{"topic_arn": row.get("topic_arn"),
                                "parent_entity_id": row.get("parent_entity_id"),
                                "entity_id": row.get("child_entity_id"),
                                "phone_number": row.get("sms_number")} for row in subscribe_data]

        # If both topic arn and subscribe data exist then only perform AWS SUBSCRIBE
        topic_subscribe_response, topic_subscribe_status_code = fn_aws_sns_topic_subscribe_multiple(kwargs,subscribe_data_json)
        if topic_subscribe_response == 1 or topic_subscribe_status_code == 400:
            response = dict()

            if topic_subscribe_response == 1:
                topic_subscribe_response = "Error in sproc_com_aws_phone_subscription_arn_dml_upd"

            response["api_oparams"] = {
                "client_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_client_entity_id", None),
                "client_sms": sproc_data[0]["Sproc_OParams"].get("oparam_client_sms", None),
                "client_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_client_whatsapp", None),
                "client_email": sproc_data[0]["Sproc_OParams"].get("oparam_client_email", None),
                "supplier_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_entity_id", None),
                "supplier_sms": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_sms", None),
                "supplier_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_whatsapp", None),
                "supplier_email": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_email", None),
                "lead_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_lead_entity_id", None),
                "lead_sms": sproc_data[0]["Sproc_OParams"].get("oparam_lead_sms", None),
                "lead_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_lead_whatsapp", None),
                "lead_email": sproc_data[0]["Sproc_OParams"].get("oparam_lead_email", None),
                "employee_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_employee_entity_id", None),
                "employee_sms": sproc_data[0]["Sproc_OParams"].get("oparam_employee_sms", None),
                "employee_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_employee_whatsapp", None),
                "employee_email": sproc_data[0]["Sproc_OParams"].get("oparam_employee_email", None)
            }
            response["api_status"] = {
                "status": "Failure",
                "message": topic_subscribe_response
            }
            response["sproc_oparams"] = {
                "oparam_err_flag": None,
                "oparam_err_step": None,
                "oparam_err_msg": None
            }

            return response, 400
        # Normal employee Insert
        else:
            response = dict()

            response["api_oparams"] = {
                "client_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_client_entity_id", None),
                "client_sms": sproc_data[0]["Sproc_OParams"].get("oparam_client_sms", None),
                "client_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_client_whatsapp", None),
                "client_email": sproc_data[0]["Sproc_OParams"].get("oparam_client_email", None),
                "supplier_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_entity_id", None),
                "supplier_sms": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_sms", None),
                "supplier_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_whatsapp", None),
                "supplier_email": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_email", None),
                "lead_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_lead_entity_id", None),
                "lead_sms": sproc_data[0]["Sproc_OParams"].get("oparam_lead_sms", None),
                "lead_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_lead_whatsapp", None),
                "lead_email": sproc_data[0]["Sproc_OParams"].get("oparam_lead_email", None),
                "employee_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_employee_entity_id", None),
                "employee_sms": sproc_data[0]["Sproc_OParams"].get("oparam_employee_sms", None),
                "employee_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_employee_whatsapp", None),
                "employee_email": sproc_data[0]["Sproc_OParams"].get("oparam_employee_email", None)
            }
            response["api_status"] = sproc_data[0].get("api_status", None)
            response["data0"] = []
            response["sproc_oparams"] = {
                "oparam_err_flag": sproc_data[0]["Sproc_OParams"].get("oparam_err_flag", None),
                "oparam_err_step": sproc_data[0]["Sproc_OParams"].get("oparam_err_step", None),
                "oparam_err_msg": sproc_data[0]["Sproc_OParams"].get("oparam_err_msg", None)
            }

            return response, 200
    else:
        response = dict()

        response["api_oparams"] = {
            "client_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_client_entity_id", None),
            "client_sms": sproc_data[0]["Sproc_OParams"].get("oparam_client_sms", None),
            "client_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_client_whatsapp", None),
            "client_email": sproc_data[0]["Sproc_OParams"].get("oparam_client_email", None),
            "supplier_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_entity_id", None),
            "supplier_sms": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_sms", None),
            "supplier_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_whatsapp", None),
            "supplier_email": sproc_data[0]["Sproc_OParams"].get("oparam_supplier_email", None),
            "lead_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_lead_entity_id", None),
            "lead_sms": sproc_data[0]["Sproc_OParams"].get("oparam_lead_sms", None),
            "lead_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_lead_whatsapp", None),
            "lead_email": sproc_data[0]["Sproc_OParams"].get("oparam_lead_email", None),
            "employee_entity_id": sproc_data[0]["Sproc_OParams"].get("oparam_employee_entity_id", None),
            "employee_sms": sproc_data[0]["Sproc_OParams"].get("oparam_employee_sms", None),
            "employee_whatsapp": sproc_data[0]["Sproc_OParams"].get("oparam_employee_whatsapp", None),
            "employee_email": sproc_data[0]["Sproc_OParams"].get("oparam_employee_email", None)
        }
        response["api_status"] = sproc_data[0].get("api_status", None)
        response["data0"] = []
        response["sproc_oparams"] = {
            "oparam_err_flag": sproc_data[0]["Sproc_OParams"].get("oparam_err_flag", None),
            "oparam_err_step": sproc_data[0]["Sproc_OParams"].get("oparam_err_step", None),
            "oparam_err_msg": sproc_data[0]["Sproc_OParams"].get("oparam_err_msg", None)
        }

        return response, 200