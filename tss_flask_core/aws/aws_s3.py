import boto3
from botocore.exceptions import ClientError
from botocore.client import Config

def put_object(key: str, body: str, bucket: str, aws_region: str = "ap-south-1"):
    s3 = boto3.client('s3', region_name=aws_region)
    try:
        s3.put_object(Bucket=bucket, Key=key, Body=body)
        return f"https://{bucket}.s3.{aws_region}.amazonaws.com/{key}"
    except ClientError as e:
        return str(e.response['Error']['Message'])
