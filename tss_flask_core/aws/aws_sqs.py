import boto3
from botocore.exceptions import ClientError

## QUEUE MANAGEMENT

sqs = boto3.client('sqs', region_name="ap-south-1")


def get_queue_url(name: str):
    try:
        return sqs.get_queue_url(QueueName=name)['QueueUrl']
    except ClientError as e:
        return str(e.response['Error']['Message'])


def create_queue(name: str):
    try:
        return sqs.create_queue(QueueName=name)
    except ClientError as e:
        return str(e.response['Error']['Message'])


def delete_queue(url: str):
    try:
        return sqs.delete_queue(QueueUrl=url)
    except ClientError as e:
        return str(e.response['Error']['Message'])


def delete_queue_by_name(name: str):
    try:
        url = get_queue_url(name)
        return delete_queue(url)
    except ClientError as e:
        return str(e.response['Error']['Message'])


def list_queues():
    try:
        return sqs.list_queues()
    except ClientError as e:
        return str(e.response['Error']['Message'])


# MESSAGES


def send_message(name: str, body: str):
    url = get_queue_url(name)
    try:
        return sqs.send_message(QueueUrl=url, MessageBody=body)
    except ClientError as e:
        return str(e.response['Error']['Message'])


def receive_message(name: str, wait_time_seconds: int = 0):
    url = get_queue_url(name)
    try:
        return sqs.receive_message(QueueUrl=url, WaitTimeSeconds=wait_time_seconds)
    except ClientError as e:
        return str(e.response['Error']['Message'])


def delete_message(name: str, receipt_handle: str):
    url = get_queue_url(name)

    try:
        return sqs.delete_message(QueueUrl=url, ReceiptHandle=receipt_handle)
    except ClientError as e:
        return str(e.response['Error']['Message'])


def purge_queue(name: str):
    url = get_queue_url(name)
    try:
        return sqs.purge_queue(QueueUrl=url)
    except ClientError as e:
        return str(e.response['Error']['Message'])

